1. buat 2 class (Hewan dan Fight), namun kedua kelas tersebut tidak bisa diinstansiasi [ berarti ini pakai trait]
2. buat kelas Elang dan Harimau, dimana kelas tersebut dapat menggunakan seluruh property dan method yang ada di kelas Hewan dan Fight. Dan kelas Elang dan Harimau ini dapat diinstansiasi [ jadiin ini class]
3. property kelas Hewan adalah nama, darah dengan nilai default 50, jumlahKaki dan keahlian
4. property kelas Fight adalah attackPower dan defencePower
5. instansiasi kelas Elang, jumlahKaki = 2 , keahlian = "terbang tinggi" , attackPower = 10 , defencePower = 5
6. instansiasi kelas Harimau, jumlahKaki = 4 , keahlian = "lari cepat" , attackPower = 7 , defencePower = 8

Method yg ada di dalam kelas Hewan:
1. atraksi(), di dalam method ini akan menampilkan string nama dan keahlian.  
   Contoh: "harimau_1 sedang lari cepat” atau “elang_3 sedang terbang tinggi"

Method yg ada di dalam kelas Fight:
1. serang(), di dalam method ini akan menampilkan string sebagai contoh berikut. 
   Contoh: "harimau_1 sedang menyerang elang_3" atau "elang_3 sedang menyerang harimau_2"

2. diserang(), di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : “harimau_1 sedang diserang” atau “elang_3 sedang diserang”, kemudian hewan yang diserang akan berkurang darahnya dengan rumus :

“darah sekarang – attackPower penyerang / defencePower yang diserang”

Method yang ada di dalam kelas Elang dan Harimau :
1. getInfoHewan(), didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau).
