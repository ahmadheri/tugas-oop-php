<?php 

trait Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function getName($nama)
    {
        return $this->nama = $nama;
    }

    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian;
    }
}

trait Fight
{
    public $attackPower;
    public $defencePower;
    public $darah_tersisa;

    public function serang($nama_penyerang, $nama_diserang)
    {
        
        return $this->getName($nama_penyerang) . " sedang menyerang " . $this->getName($nama_diserang);
    }

    public function diserang($nama, $attackPower_penyerang, $defencePower_diserang)
    {
        $darah_tersisa = $this->darah - $attackPower_penyerang / $defencePower_diserang;

        $string = "Darah " . $this->getName($nama) .  " tersisa " .  $darah_tersisa;
        
        $this->darah_tersisa = $darah_tersisa;

        return $string;
    }
}

class Elang
{
    use Hewan, Fight;

    public $jenis_hewan = 'Elang';

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo 'Nama: ' . $this->nama . '<br>';
        echo 'Darah sisa: ' . $this->darah_tersisa . '<br>';
        echo 'Jumlah Kaki: ' . $this->jumlahKaki . '<br>';
        echo 'Keahlian: ' . $this->keahlian . '<br>';
        echo 'Attack: ' . $this->attackPower . '<br>';
        echo 'Defence: ' . $this->defencePower . '<br>';
        echo 'Jenis hewan: ' . $this->jenis_hewan . '<br>';
    }

}

class Harimau
{
    use Hewan, Fight;

    public $jenis_hewan = 'Harimau';

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo 'Nama: ' . $this->nama . '<br>';
        echo 'Darah sisa: ' . $this->darah_tersisa . '<br>';
        echo 'Jumlah Kaki: ' . $this->jumlahKaki . '<br>';
        echo 'Keahlian: ' . $this->keahlian . '<br>';
        echo 'Attack: ' . $this->attackPower . '<br>';
        echo 'Defence: ' . $this->defencePower . '<br>';
        echo 'Jenis hewan: ' . $this->jenis_hewan . '<br>';
    }

}

$elang = new Elang("elang_sakti", 2, "terbang tinggi", 10, 5);
$harimau = new Harimau("harimau_buas", 4, "lari cepat", 7, 8);

echo $elang->atraksi() . '<br>'; 
echo $harimau->atraksi() . '<br><br>'; 

echo $elang->serang("elang_sakti", "harimau_buas") . '<br>';
echo $harimau->serang("harimau_buas", "elang_sakti") . '<br><br>';

echo $elang->diserang("elang_sakti", $harimau->attackPower, $elang->defencePower) . '<br>';
echo $harimau->diserang("harimau_buas", $elang->attackPower, $harimau->defencePower) . '<br><br>';

echo $elang->getInfoHewan() . '<br>';
echo $harimau->getInfoHewan();
















